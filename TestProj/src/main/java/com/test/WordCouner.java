package com.test;

import java.util.HashMap;

/**
 * Created by Sergey on 10.06.2015.
 */
public class WordCouner {

    HashMap<String,Integer> words = new HashMap<>();

    public int countWord(String word) {
        Integer count = words.get(word);
        if(count == null){
            return 0;
        }
        return count;
    }

    public static final int MIN_WORD_LENGHT = 2;

    public void accept(String word) {
        assert word.length() > MIN_WORD_LENGHT;
        words.put(word,countWord(word)+1);
    }
}
