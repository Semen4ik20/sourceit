import com.test.WordCouner;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by Sergey on 10.06.2015.
 */
public class WordCounterTest {

    WordCouner wordCouner = new WordCouner();

    @Test
    public void shouldBeZeroIfNotAccepted() {
        int curr = wordCouner.countWord("word");
        assertThat(curr, equalTo(0));
    }

    @Test
    public void shouldAcceptWord(){
        wordCouner.accept("word");
        int curr = wordCouner.countWord("word");
        assertThat(curr, equalTo(1));
    }

    @Test
    public void shouldBeZeroWhenOtherWordAccepted(){
        wordCouner.accept("word");
        int curr = wordCouner.countWord("asdasd");
        assertThat(curr, equalTo(0));
    }

    @Test
    public void shouldBeErrorIfWordLessThenTwoSymbols(){
        wordCouner.accept("ww3");
    }

}
