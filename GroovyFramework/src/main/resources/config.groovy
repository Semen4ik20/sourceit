/**
 * Created by Serhii_Pirohov on 11.06.2015.
 */
config {
    local {
        hubAddress = 'http://localhost:4444/wd/hub'
        browser = 'chrome'
    }
    prod {
        hubAddress = 'http://localhost:4444/wd/hub'
        browser = 'iexplorer'
    }
}
