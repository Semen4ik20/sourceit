package com.demo.application.pages

import static com.codeborne.selenide.Selenide.*

/**
 * Created by Serhii_Pirohov on 11.06.2015.
 */
class MainPage {

    def static URL = "http://ukr.net/"


    def userNameInpit(){
        $('.login>input')
    }

    def userPasswordInput(){
        $('.password>input')
    }

    def loginBtn(){
        $('.submit>button')
    }

    def loginErrorMessage() {
        $('.error-text')
    }
}
