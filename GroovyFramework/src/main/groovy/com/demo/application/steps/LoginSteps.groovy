package com.demo.application.steps
import com.codeborne.selenide.Condition
import com.demo.application.pages.MainPage
/**
 * Created by Serhii_Pirohov on 16.06.2015.
 */
class LoginSteps {

    MainPage mainPage;

    LoginSteps() {
        mainPage = new MainPage()
    }

    def loginAs(String uName,String pass){
        mainPage.userNameInpit().setValue(uName)
        mainPage.userPasswordInput().setValue(pass)
        mainPage.loginBtn().click()
    }

    def shouldBeErrorMessage(String msg, Condition condition) {
        def message = mainPage.loginErrorMessage()
        message.shouldHave(msg,condition)
    }
}

