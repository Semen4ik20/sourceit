package com.demo.framework

import com.codeborne.selenide.WebDriverRunner
import com.demo.framework.configuration.Config
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.RemoteWebDriver

import static com.demo.framework.factory.CapabilitiesFactory.getCapabilities
/**
 * Created by Serhii_Pirohov on 16.06.2015.
 */
class Core {

    def static setUp(){
        def config = new Config().getConfig()
        def cap = getCapabilities(config.browser);
        WebDriver driver = new RemoteWebDriver(new URL(config.hubAddress),cap)
        WebDriverRunner.setWebDriver(driver)
    }

}

