package com.demo.framework.factory

import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
/**
 * Created by Serhii_Pirohov on 15.06.2015.
 */
enum DriverFactory {

    REMOTE{
        def create(String address){
            return new RemoteWebDriver(new URL(address),DesiredCapabilities.internetExplorer())
        }
    }


    def create(){
        return null;
    }


}