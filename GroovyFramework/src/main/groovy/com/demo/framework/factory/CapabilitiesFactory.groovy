package com.demo.framework.factory

import org.openqa.selenium.remote.DesiredCapabilities

/**
 * Created by Serhii_Pirohov on 16.06.2015.
 */
class CapabilitiesFactory {

    public static DesiredCapabilities getCapabilities(String browser){
        if("IE".equalsIgnoreCase(browser)){
            return DesiredCapabilities.internetExplorer()
        }else if("CHROME".equalsIgnoreCase(browser)){
            return DesiredCapabilities.chrome()
        }else{
            return DesiredCapabilities.firefox()
        }
    }

}
