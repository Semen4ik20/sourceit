package com.demo.framework.configuration

/**
 * Created by Serhii_Pirohov on 15.06.2015.
 */
class Config {

    final String CONFIG_FILE = 'src/main/resources/config.groovy'
    final String ENVIRONMENTS = 'config'

    def environment = System.getProperties().getProperty('env')

    def read() {
        new File(CONFIG_FILE).text
    }

    def getEnv(String envName) {
        def config = new ConfigSlurper(envName)
        config.registerConditionalBlock(ENVIRONMENTS, envName)
        config.parse(read())
    }

    def getConfig(){
        getEnv(environment)
    }
}
