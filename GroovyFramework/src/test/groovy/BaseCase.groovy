import com.codeborne.selenide.Selenide
import com.demo.framework.Core
import org.junit.AfterClass
import org.junit.BeforeClass

/**
 * Created by Serhii_Pirohov on 16.06.2015.
 */
abstract class BaseCase {

    @BeforeClass
    public static void  setUp() {
        Core.setUp()
    }

    @AfterClass
    public static void tearDown(){
        Selenide.close()
    }
}
