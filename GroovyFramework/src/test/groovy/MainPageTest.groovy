import com.codeborne.selenide.Selenide
import com.demo.application.pages.MainPage
import com.demo.application.steps.LoginSteps
import org.junit.BeforeClass
import org.junit.Test

import static com.codeborne.selenide.Condition.visible
/**
 * Created by Serhii_Pirohov on 11.06.2015.
 */
class MainPageTest extends BaseCase{

    static LoginSteps login;

    @BeforeClass
    public static void init(){
        login = new LoginSteps()
    }

    @Test
    public void shouldBeErrorMessage() {
        Selenide.open(MainPage.URL)
        login.loginAs("asdasd","asdasdasd")
        login.shouldBeErrorMessage("Не вірний логін або пароль. Спробуйте знову.",visible)
    }
}
